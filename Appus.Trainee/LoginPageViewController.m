//
//  LoginPageViewController.m
//  TraineeProj
//
//  Created by Andrey Brizhnichenko on 6/29/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "LoginPageViewController.h"
#import "User.h"
#import "SidebarViewController.h"
#import "SessionManager.h"

@implementation LoginPageViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [_EmailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    _EmailTextField.delegate = self;
    _PassTextField.delegate = self;
}

- (IBAction)emailEditingDidBegin:(id)sender {
    [_EmailTextField becomeFirstResponder];
}

- (IBAction)emalEditingDidEnd:(id)sender {
    [_EmailTextField resignFirstResponder];
}

- (IBAction)onLoginClick:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SidebarViewController *sbVC = [storyboard instantiateViewControllerWithIdentifier:@"Reveal View Controller"];
    LoginHandler* aph = [[LoginHandler alloc] init];
    NSString *userEmail = _EmailTextField.text;
    NSString *userPass = _PassTextField.text;
    
        if([userEmail containsString:@"@"] && [userEmail containsString:@"."] && userEmail.length >= 5 && userPass.length >= 5)
        {
            sbVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [aph userLogin : userEmail : userPass];
            [aph statusChange];
            if (aph.didSendRequest) {
                NSLog(@"%@ RESPONSE HERE", aph.jsonDictionary);
                User* user = [[User alloc] initFromDictionary: aph.jsonDictionary];
                SessionManager *sharedInstance = [SessionManager sharedInstance];
                sharedInstance.user = user;
                //[self presentViewController:sbVC animated:YES completion:NULL];
            }
        }

}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.EmailTextField || theTextField == self.PassTextField) {
        [theTextField resignFirstResponder];
    }
    return YES;
}

- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = self.RegisterButton.frame.origin;
    
    CGFloat buttonHeight = self.RegisterButton.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        
        [self.scroller setContentOffset:scrollPoint animated:YES];
        
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    [self.scroller setContentOffset:CGPointZero animated:YES];
    
}

@end
