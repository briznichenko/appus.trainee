//
//  AppusAPIDelegate.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/4/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AppsAPIDelegate
- (void)receivedUserJSON:(NSData *)objectNotation;
- (void)fetchingUserFailedWithError:(NSError *)error;
@end