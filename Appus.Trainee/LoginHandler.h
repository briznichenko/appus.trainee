//
//  AppusApiHandler.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/6/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "User.h"

@interface LoginHandler : NSObject

@property (retain, nonatomic) NSMutableData *receivedData;
@property (retain, nonatomic) NSURLConnection *connection;
@property (nonatomic, retain) User *user;
@property (atomic, strong) NSDictionary* jsonDictionary;
@property (nonatomic, assign) BOOL didSendRequest;

-(void)userRegister : NSString : NSString;
-(void)userLogin : NSString : NSString;
-(void)forgotPassword : NSString;
-(NSDictionary*) returnJSON;
-(void) statusChange;
@end
