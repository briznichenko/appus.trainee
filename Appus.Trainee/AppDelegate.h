//
//  AppDelegate.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 6/29/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

- (void)startupAnimationDone:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

@property (strong, nonatomic) UIWindow *window;


@end

