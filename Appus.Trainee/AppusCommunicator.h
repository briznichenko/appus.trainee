//
//  AppusCommuicator.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/4/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@protocol AppusAPIDelegate;

@interface AppusCommunicator : NSObject

@property (weak, nonatomic) id<AppusAPIDelegate> delegate;

-(void) getUser : (NSString*) email : password;
-(void) addUser : (NSString*) email : password;
-(void) establishConnection : (NSString*) url;

@end
