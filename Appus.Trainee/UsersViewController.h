//
//  UsersViewController.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/5/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsersViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
