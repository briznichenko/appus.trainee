//
//  UserMapViewController.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 6/30/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface PostsViewController : UIViewController   <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end

