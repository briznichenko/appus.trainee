//
//  AppusApiHandler.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/6/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "LoginHandler.h"
#import <AFNetworking/AFNetworking.h>

@implementation LoginHandler


-(void)userRegister : (NSString* )email : (NSString*) password{
    NSURL *url = [NSURL URLWithString:@"http://traineeship.appus.work/api/register"];
    NSDictionary *userParams = [[NSDictionary alloc] initWithObjectsAndKeys:
                        email,@"email",
                        password,@"password",
                         nil];
    [self postRequest: url :userParams];
}

-(void)userLogin : (NSString* )email : (NSString*) password {
    NSURL *url = [NSURL URLWithString:@"http://traineeship.appus.work/api/login"];
    NSDictionary *userParams = [[NSDictionary alloc] initWithObjectsAndKeys:
                                email,@"email",
                                password,@"password",
                                nil];
    [self postRequest: url :userParams];
    
    }

-(void) forgotPassword:(NSString*) email{
    NSURL *url = [NSURL URLWithString:@"http://traineeship.appus.work/api/reset"];
    NSDictionary *userParams =  [[NSDictionary alloc] initWithObjectsAndKeys:
                                 email,@"email",
                                 nil];

    [self postRequest: url :userParams];
}

-(void) postRequest: (NSURL*) url : (NSDictionary*) params{
    [self.connection cancel];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
    NSString *postString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    NSLog(@"Request: %@", postString);
    NSData *requestData = [postString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL: url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionDataTask *postTask = [[NSURLSession sharedSession]
                                          dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if(!error)
                                              {
                                                  self.jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                                                  NSLog(@"RESPONSE DATA:  %@", response);
                                                  self.didSendRequest = YES;
                                              }
                                              else
                                              {
                                                  self.didSendRequest = NO;
                                                  NSLog(@"ERROR:  %@", error);
                                              }
                                          }];
    self.didSendRequest = YES;
    [postTask resume];
    

}

-(NSDictionary *)returnJSON
{
    if(!self.jsonDictionary){
        return nil;
    }
    return self.jsonDictionary;
}

-(void) statusChange{
    while(!self.didSendRequest){
        NSLog(@"Waiting...");
        if(self.didSendRequest) break;
    }
}

@end
