//
//  ForgotPassViewController.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 6/30/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "ForgotPassViewController.h"
#import "LoginHandler.h"
#import "LoginPageViewController.h"

@interface ForgotPassViewController ()

@end

@implementation ForgotPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.EmailTextField.delegate = self;
}

- (IBAction)resetPassBttnClick:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    LoginPageViewController *lpVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginPage"];
    LoginHandler* aph = [[LoginHandler alloc] init];
    lpVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    NSDictionary* response;
    NSString *userEmail = _EmailTextField.text;

        if([userEmail containsString:@"@"] && [userEmail containsString:@"."] && userEmail.length >= 5)
        {
            [aph forgotPassword: userEmail];
            [aph statusChange];
            if (aph.didSendRequest) {
                response = aph.jsonDictionary;
                NSLog(@"%@", response);
                [self presentViewController:lpVC animated:YES completion:NULL];
            }
        }
 

    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //hides keyboard when another part of layout was touched
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.EmailTextField) {
        [theTextField resignFirstResponder];
    }
    return YES;
}

- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = self.ResetPassButton.frame.origin;
    
    CGFloat buttonHeight = self.ResetPassButton.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        
        [self.scroller setContentOffset:scrollPoint animated:YES];
        
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    [self.scroller setContentOffset:CGPointZero animated:YES];
    
}

@end
