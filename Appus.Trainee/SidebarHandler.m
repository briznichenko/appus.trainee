//
//  SidebarHandler.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/6/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "SidebarHandler.h"
#import "SWRevealViewController.h"

@interface SidebarHandler ()

@end

@implementation SidebarHandler

- (void) associateWithMenu : (UIBarButtonItem*) sidebarButton
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}



@end
