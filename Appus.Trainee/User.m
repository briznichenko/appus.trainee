//
//  User.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/4/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "User.h"


@implementation User



-(void) printUserInfo{
    NSLog(@"%@, %@, %@, %@, %@, %@, %@, %@, %@, %@", self.user_id, self.email, self.password, self.username, self.avatar, self.fb_id, self.fb_username, self.tw_id, self.tw_username, self.session);
}

-(instancetype)initFromDictionary:(NSDictionary *)userParameters{
    if (self = [super init]) {
        self.user_id = userParameters[@"id"];
        self.email = userParameters[@"email"];
        self.password = userParameters[@"password"];
        self.username = userParameters[@"username"];
        self.avatar= userParameters[@"avatar"];
        self.fb_id = userParameters[@"fb_id"];
        self.fb_username = userParameters[@"fb_username"];
        self.tw_id = userParameters[@"tw_id"];
        self.tw_username = userParameters[@"tw_username"];
        self.session = userParameters[@"session"];
        
        [self printUserInfo];
    }
    return self;
}

@end
