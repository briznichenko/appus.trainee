//
//  ForgotPassViewController.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 6/30/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *EmailTextField;
@property (strong, nonatomic) IBOutlet UIButton *ResetPassButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@end
