//
//  SessionManager.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/7/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "SessionManager.h"

@implementation SessionManager

@synthesize user;

#pragma mark Singleton Methods

static SessionManager *sharedInstance = nil;

+ (SessionManager *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}


- (instancetype)init {
    self = [super init];
    if (self) {
        return self;
    }
    return self;
}

    
@end
