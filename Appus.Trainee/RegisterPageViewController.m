//
//  RegisterPageViewController.m
//  TraineeProj
//
//  Created by Andrey Brizhnichenko on 6/29/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "RegisterPageViewController.h"
#import "SidebarViewController.h"
#import "LoginHandler.h"
#import "SessionManager.h"

@interface RegisterPageViewController ()

@end

@implementation RegisterPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    _errors = @"";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onEmailEditingEnd:(id)sender {
    NSString *userEmail = [_EmailTextField text];
    if ((![userEmail containsString:@"@"] && [userEmail containsString:@"."]) || userEmail.length < 5) _errors = [_errors stringByAppendingString:@"Not a valid email"];
    else _errors  = [_errors stringByAppendingString:@""];
    _passDontMatchNotification.text = _errors;
}


-(void) onPass2Input:(id)sender{
    NSString *userPass1 = [_PassTextField text];
    NSString *userPass2 = [_ConfirmPassTextField text];
    if (userPass1 != userPass2) _errors  = [_errors stringByAppendingString:@"Password doesn't match"];
    else _errors  = [_errors stringByAppendingString:@""];
    _passDontMatchNotification.text = _errors;
}

- (IBAction)onRegisterClick:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SidebarViewController *sbVC = [storyboard instantiateViewControllerWithIdentifier:@"Reveal View Controller"];
    sbVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    LoginHandler *aph = [[LoginHandler alloc] init];
    NSDictionary *response;
    NSString *userEmail = [_EmailTextField text];
    NSString *userPass1 = [_PassTextField text];
    NSString *userPass2 = [_ConfirmPassTextField text];
    if (userPass1 == userPass2)
    {
        [aph userRegister : userEmail : userPass1];
        [aph statusChange];
        if (aph.didSendRequest) {
            response = aph.jsonDictionary;
            NSLog(@"%@", response);
            User* user = [[User alloc] initFromDictionary: response];
            SessionManager *sharedInstance = [SessionManager sharedInstance];
            [self presentViewController:sbVC animated:YES completion:NULL];
        }
    }
    else _passDontMatchNotification.text = @"Passwords don't match";
}
        
    
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.EmailTextField || theTextField == self.PassTextField || theTextField == self.ConfirmPassTextField) {
        [theTextField resignFirstResponder];
    }
    return YES;
}

- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = self.RegisterButton.frame.origin;
    
    CGFloat buttonHeight = self.RegisterButton.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        
        [self.scroller setContentOffset:scrollPoint animated:YES];
        
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    [self.scroller setContentOffset:CGPointZero animated:YES];
    
}

@end
