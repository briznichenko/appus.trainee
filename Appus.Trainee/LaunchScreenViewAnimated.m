//
//  LaunchScreenViewAnimated.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/4/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "LaunchScreenViewAnimated.h"
#import "UIImage+animatedGIF.h"

@implementation LaunchScreenViewAnimated

-(void)drawRect:(CGRect)rect{
    _mygif = [UIImage animatedImageWithAnimatedGIFURL:[NSURL URLWithString:@"Splash_iPhone4.gif"]];
}



/*
 UIImageView* animatedImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
 animatedImageView.animationImages = [NSArray arrayWithObjects:
 [UIImage imageNamed:@"Splash_iPhone4.gif"],nil];
 animatedImageView.animationDuration = 100.0f;
 animatedImageView.animationRepeatCount = 0;
 [animatedImageView startAnimating];
 [self.view addSubview: animatedImageView];
*/

@end
