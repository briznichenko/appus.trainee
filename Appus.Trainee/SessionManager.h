//
//  SessionManager.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/7/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface SessionManager : NSObject{ User *user;}

@property (nonatomic, retain) User *user;
@property (atomic, strong) NSDictionary *userParams;

+ (SessionManager *)sharedInstance;

@end
