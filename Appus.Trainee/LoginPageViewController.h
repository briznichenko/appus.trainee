//
//  LoginPageViewController.h
//  TraineeProj
//
//  Created by Andrey Brizhnichenko on 6/29/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LoginHandler.h"

@interface LoginPageViewController : UIViewController <UITextFieldDelegate>;

@property (strong, nonatomic) IBOutlet UITextField *EmailTextField;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UITextField *PassTextField;
@property (strong, nonatomic) IBOutlet UIButton *LoginButton;
@property (strong, nonatomic) IBOutlet UIButton *RegisterButton;

@end
