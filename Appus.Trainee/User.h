//
//  User.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/4/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(atomic, weak) NSString *user_id;
@property(atomic, weak) NSString *email;
@property(atomic, weak) NSString *password;
@property(atomic, weak) NSString *username;
@property(atomic, weak) NSString *avatar;
@property(atomic, weak) NSString *fb_id;
@property(atomic, weak) NSString *fb_username;
@property(atomic, weak) NSString *tw_id;
@property(atomic, weak) NSString *tw_username;
@property(atomic, weak) NSString *session;

-(void) printUserInfo;
-(instancetype) initFromDictionary: (NSDictionary*) userParameters NS_DESIGNATED_INITIALIZER;

@end
