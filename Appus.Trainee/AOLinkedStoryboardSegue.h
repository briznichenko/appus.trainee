//
//  AOLinkedStoryboardSegue.h
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 7/5/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AOLinkedStoryboardSegue : UIStoryboardSegue

+ (UIViewController *)sceneNamed:(NSString *)identifier;

@end
