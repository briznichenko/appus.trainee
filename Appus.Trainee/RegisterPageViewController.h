//
//  RegisterPageViewController.h
//  TraineeProj
//
//  Created by Andrey Brizhnichenko on 6/29/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterPageViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *EmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *PassTextField;
@property (strong, nonatomic) IBOutlet UITextField *ConfirmPassTextField;
@property (strong, nonatomic) IBOutlet UIButton *RegisterButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UILabel *passDontMatchNotification;
@property (strong, nonatomic) NSString* errors;

- (IBAction)onPass2Input:(id)sender;
- (IBAction)onRegisterClick:(id)sender;

@end
