//
//  UserMapViewController.m
//  Appus.Trainee
//
//  Created by Andrey Brizhnichenko on 6/30/16.
//  Copyright © 2016 Andrey Brizhnichenko. All rights reserved.
//

#import "PostsViewController.h"
#import "SWRevealViewController.h"

@interface PostsViewController ()

@end

@implementation PostsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





@end
